console.log(document);

//Targetting the first name input failed
// document.querySelector('#txt-first-name');


//alternatively:
// document.getElementById('txt-first-name');

//Contain the query selector code in a constant
const txtFirstName = document.querySelector('#txt-first-name');

const txtLastName = document.querySelector('#txt-last-name');
// console.log(txtFirstName);

const spanFullName = document.querySelector('#span-full-name');

// Event Listeners
// txtFirstName.addEventListener('keyup', (event) => {
//  	spanFullName.innerHTML = txtFirstName.value;
// });

// Multiple Listeners
// txtFirstName.addEventListener('keyup', (e) => {
// 	console.log(e.target);
// 	console.log(e.target.value);
// });

//ACTIVITY

const printFullName = () => {
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
	// spanFullName.innerHTML = txtLastName.value;
}

addEventListener('keyup', printFullName);